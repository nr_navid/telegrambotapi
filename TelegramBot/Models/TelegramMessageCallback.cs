﻿namespace TelegramBot.Models
{
    public class TelegramMessageCallback
    {
        public string ChatID { get; set; }
        public string Message { get; set; }
    }
}
