﻿using NavidCoreTools.Models.Telegram;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelegramBot.Models
{
    public class TelegramMessage : TelegramMessageBase
    {
        public TelegramMessage()
        {
            CreatedTime = DateTime.Now;
        }
        public TelegramMessage(TelegramMessageBase telegramMessageBase)
        {
            this.IsActive = true;
            this.Message = telegramMessageBase.Message;
            this.Retries = 0;
            this.EnviromentType = telegramMessageBase.EnviromentType;
            this.Sent = false;
            this.EnvironmentID = telegramMessageBase.EnvironmentID;
            this.MessageType = telegramMessageBase.MessageType;
            this.WaitForSend = telegramMessageBase.WaitForSend;
            this.OneTimeOnly = telegramMessageBase.OneTimeOnly;
            this.Base64Image = telegramMessageBase.Base64Image;
            this.HasImage = telegramMessageBase.HasImage;
            this.TelegramBotName = telegramMessageBase.TelegramBotName;
            this.TelegramButtonsGroup = telegramMessageBase.TelegramButtonsGroup;
        }
        private bool _Sent;
        public DateTime? LastSentTime { get; set; }
        public int Retries { get; set; }
        public bool IsActive { get; set; }
        public bool Sent
        {
            get
            {
                return _Sent;
            }
            set
            {
                if (value)
                {
                    IsActive = false;
                    _Sent = value;
                }
            }
        }
    }
}
