﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelegramBot.Models
{
    public class TelegramBotConfig
    {
        public string TelegramBotToken { get; set; }
        public string TelegramBotName { get; set; }
        public long DefaultChatID { get; set; }
        public string DefaultUserName { get; set; }
        public string DefaultChannelName { get; set; }
        public long DefaultUserId { get; set; }
        public long DefaultPrivateChannelID { get; set; }
        public string ApplicationToken { get; set; }
    }
}
