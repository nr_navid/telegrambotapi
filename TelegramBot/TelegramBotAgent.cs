﻿using Microsoft.Extensions.Logging;
using NavidCoreTools.Enums;
using NavidCoreTools.Enums.Telegram;
using NavidCoreTools.Models.Telegram;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramBot.Models;

namespace TelegramBot
{
    public class TelegramBotAgent : IDisposable
    {
        private readonly TelegramBotConfig telegromBotConfig;
        private readonly ILogger logger;
        public event EventHandler<Message> OnMessage;
        public event EventHandler<Message> OnMessageEdited;
        private TelegramBotClient telegramBotClient;
        private UpdateHandler updateHandler;
        public bool MustWait { get; set; }
        private readonly SemaphoreSlim semaphoreSlim = new SemaphoreSlim(1, 1);
        public event EventHandler<TelegramMessageCallback> OnMessageReceived;
        public TelegramBotAgent(TelegramBotConfig telegromBotConfig, ILogger logger)
        {
            this.telegromBotConfig = telegromBotConfig;
            this.logger = logger;
            telegramBotClient = new TelegramBotClient(telegromBotConfig.TelegramBotToken);
        }
        public void HookUpListening()
        {
            updateHandler = new UpdateHandler(logger);
            updateHandler.OnMessageRecived += UpdateHandler_OnMessageRecived;
            telegramBotClient.StartReceiving(updateHandler);
        }

        private void UpdateHandler_OnMessageRecived(object? sender, TelegramMessageCallback e)
        {
            OnMessageReceived?.Invoke(this, e);
        }

        private async Task CreateHomeWelcomeMessageAsync(long chatid, string name)
        {
            KeyboardButton keyboardButton = new KeyboardButton(Emoji.House);
            var message = await telegramBotClient.SendTextMessageAsync(new ChatId(chatid), $"Hello {name} Welcome To Navid RJ Telegram Bot ;)", replyMarkup: new ReplyKeyboardMarkup(keyboardButton) { ResizeKeyboard = true });
            //await telegramBotClient.EditMessageReplyMarkupAsync(message.Chat.Id ,message.MessageId, );
        }
        private async Task CreateHomeMenuAsync(long chatid)
        {
            await telegramBotClient.SendTextMessageAsync(new ChatId(chatid), $"{Emoji.Iphone} Command Menu{Environment.NewLine}", replyMarkup: new InlineKeyboardMarkup(new InlineKeyboardButton("Get My Chat Code") { CallbackData = "GetMyConversationCode" }));
        }
        private TOut GenereteTelegramKeyBoard<TOut>(TelegramButton telegramButton)
        {
            if (typeof(TOut) == typeof(KeyboardButton))
            {
                return (TOut)Convert.ChangeType(new KeyboardButton(telegramButton.OptionText)
                {
                    RequestLocation = telegramButton.TelegramButtonSubType == TelegramButtonSubType.KeyBoardLocation,
                    RequestContact = telegramButton.TelegramButtonSubType == TelegramButtonSubType.KeyboardContact
                }, typeof(TOut));
            }
            if (typeof(TOut) == typeof(InlineKeyboardButton))
            {
                return (TOut)Convert.ChangeType(new InlineKeyboardButton(telegramButton.OptionText)
                {
                    CallbackData = (telegramButton.TelegramButtonSubType == TelegramButtonSubType.InlineCallBack ? telegramButton.OptionVaue : null),
                    Url = (telegramButton.TelegramButtonSubType == TelegramButtonSubType.InlineURL ? telegramButton.OptionVaue : null)

                }, typeof(TOut));
            }
            return (TOut)Convert.ChangeType(null, typeof(TOut));
        }
        private IReplyMarkup CreateReplyMarkUp(TelegramButtonsGroup telegramButtonsGroup)
        {
            IReplyMarkup replyMarkup = null;
            if (telegramButtonsGroup is null || telegramButtonsGroup.TelegramButtons.Count == 0)
            {
                return replyMarkup;
            }
            if (telegramButtonsGroup.TelegramButtons.Count == 1)
            {
                var telegramButton = telegramButtonsGroup.TelegramButtons.FirstOrDefault();
                switch (telegramButtonsGroup.TelegramButtonType)
                {
                    case TelegramButtonType.InlineKeyboard:
                        replyMarkup = new InlineKeyboardMarkup(GenereteTelegramKeyBoard<InlineKeyboardButton>(telegramButton));
                        break;
                    case TelegramButtonType.ReplyKeyboard:
                        replyMarkup = new ReplyKeyboardMarkup(GenereteTelegramKeyBoard<KeyboardButton>(telegramButton));
                        break;
                }
                return replyMarkup;
            }
            else
            {
                bool gotMoreY = false;
                dynamic buttonsX;
                if (telegramButtonsGroup.TelegramButtons.Max(p => p.Y_Pos > 1))
                {
                    gotMoreY = true;
                    switch (telegramButtonsGroup.TelegramButtonType)
                    {
                        case TelegramButtonType.InlineKeyboard:
                            buttonsX = new List<List<InlineKeyboardButton>>();
                            break;
                        case TelegramButtonType.ReplyKeyboard:
                            buttonsX = new List<List<KeyboardButton>>();
                            break;
                        default:
                            buttonsX = null;
                            break;
                    }
                }
                else
                {
                    switch (telegramButtonsGroup.TelegramButtonType)
                    {
                        case TelegramButtonType.InlineKeyboard:
                            buttonsX = new List<InlineKeyboardButton>();
                            break;
                        case TelegramButtonType.ReplyKeyboard:
                            buttonsX = new List<KeyboardButton>();
                            break;
                        default:
                            buttonsX = null;
                            break;
                    }
                }

                var groups = telegramButtonsGroup.TelegramButtons.GroupBy(p => p.Y_Pos, (key, item) => new { key, telegramButtonOptions = item }).OrderBy(p => p.key);
                foreach (var itemy in groups)
                {
                    dynamic buttonsY;
                    if (gotMoreY)
                    {
                        switch (telegramButtonsGroup.TelegramButtonType)
                        {
                            case TelegramButtonType.InlineKeyboard:
                                buttonsY = new List<InlineKeyboardButton>();
                                break;
                            case TelegramButtonType.ReplyKeyboard:
                                buttonsY = new List<KeyboardButton>();
                                break;
                            default:
                                buttonsY = null;
                                break;
                        }
                    }
                    else
                    {
                        switch (telegramButtonsGroup.TelegramButtonType)
                        {
                            case TelegramButtonType.InlineKeyboard:
                                buttonsY = new InlineKeyboardButton("");
                                break;
                            case TelegramButtonType.ReplyKeyboard:
                                buttonsY = new InlineKeyboardButton("");
                                break;
                            default:
                                buttonsY = null;
                                break;
                        }

                    }
                    foreach (var itemx in itemy.telegramButtonOptions)
                    {
                        switch (telegramButtonsGroup.TelegramButtonType)
                        {
                            case TelegramButtonType.InlineKeyboard:
                                if (gotMoreY)
                                {
                                    buttonsY.Add(GenereteTelegramKeyBoard<InlineKeyboardButton>(itemx));
                                }
                                else
                                {
                                    buttonsY = GenereteTelegramKeyBoard<InlineKeyboardButton>(itemx);
                                }
                                break;
                            case TelegramButtonType.ReplyKeyboard:
                                if (gotMoreY)
                                {
                                    buttonsY.Add(GenereteTelegramKeyBoard<KeyboardButton>(itemx));
                                }
                                else
                                {
                                    buttonsY = GenereteTelegramKeyBoard<KeyboardButton>(itemx);
                                }
                                break;
                        }
                        if (!gotMoreY)
                        {
                            buttonsX.Add(buttonsY);
                        }
                    }
                    if (gotMoreY)
                    {
                        buttonsX.Add(buttonsY);
                    }
                }
                replyMarkup = new InlineKeyboardMarkup(buttonsX);
                return replyMarkup;
            }
        }
        public async Task<bool> SendMessageAsync(TelegramMessage telegramMessage)
        {
            await semaphoreSlim.WaitAsync();
            try
            {
                ChatId chatId = null;
                switch (telegramMessage.EnviromentType)
                {
                    case TelegramEnviromentType.PrivateChannel:
                    case TelegramEnviromentType.Group:
                        long id = 0;
                        if (telegramMessage.EnvironmentID != string.Empty)
                            long.TryParse(telegramMessage.EnvironmentID, out id);
                        else
                            id = (telegramMessage.EnviromentType == TelegramEnviromentType.PrivateChannel ? telegromBotConfig.DefaultPrivateChannelID : telegromBotConfig.DefaultChatID);
                        chatId = new ChatId(id);
                        break;
                    case TelegramEnviromentType.User:
                    case TelegramEnviromentType.Channel:
                        string item = (telegramMessage.EnvironmentID != string.Empty ? telegramMessage.EnvironmentID : (telegramMessage.EnviromentType == TelegramEnviromentType.Channel ? telegromBotConfig.DefaultChannelName : telegromBotConfig.DefaultUserName));
                        chatId = new ChatId(telegramMessage.EnvironmentID);
                        break;
                }
                telegramMessage.LastSentTime = DateTime.Now;
                await Task.Delay(new Random().Next(5, 95));
                Message TMessage = null;
                if (telegramMessage.HasImage && !string.IsNullOrEmpty(telegramMessage.Base64Image))
                {
                    MemoryStream base64MemoryStream = new MemoryStream(Convert.FromBase64String(telegramMessage.Base64Image));
                    MemoryStream bitmapMemoryStream = new MemoryStream();
                    try
                    {
                        //byte[] bytes = Convert.FromBase64String(base64ImageRepresentation);

                        //using (MemoryStream ms = new MemoryStream(bytes))
                        //{
                        IronSoftware.Drawing.AnyBitmap bitmap = new IronSoftware.Drawing.AnyBitmap(base64MemoryStream);
                        bitmapMemoryStream = bitmap.ToStream();
                        //(Image.FromStream(base64MemoryStream) as Bitmap).Save(bitmapMemoryStream, System.Drawing.Imaging.ImageFormat.Png);
                        bitmapMemoryStream.Position = 0;
                        TMessage = await telegramBotClient.SendPhotoAsync(chatId, new Telegram.Bot.Types.InputFileStream(bitmapMemoryStream, "chartDetails"), caption: telegramMessage.Message, replyMarkup: CreateReplyMarkUp(telegramMessage.TelegramButtonsGroup));
                        await bitmapMemoryStream.DisposeAsync();
                        await base64MemoryStream.DisposeAsync();
                    }
                    catch (Exception ex)
                    {
                        await bitmapMemoryStream.DisposeAsync();
                        await base64MemoryStream.DisposeAsync();
                        logger.LogError(ex.Message);
                        await telegramBotClient.SendTextMessageAsync(chatId, ex.Message, replyMarkup: CreateReplyMarkUp(telegramMessage.TelegramButtonsGroup));

                    }
                }
                else
                {
                    TMessage = await telegramBotClient.SendTextMessageAsync(chatId, telegramMessage.Message, replyMarkup: CreateReplyMarkUp(telegramMessage.TelegramButtonsGroup));
                }
                if (telegramMessage.PinnedMessage)
                {
                    await telegramBotClient.PinChatMessageAsync(chatId, TMessage.MessageId);
                }
                telegramMessage.TelegramMessageID = TMessage.MessageId;
                telegramMessage.Sent = true;
            }
            catch (Exception ex)
            {
                semaphoreSlim.Release();
                if (telegramMessage.Retries++ > 10)
                {
                    telegramMessage.IsActive = false;
                    return false;
                }
                else
                {
                    return await SendMessageAsync(telegramMessage);
                }
            }
            finally
            {
                if (semaphoreSlim.CurrentCount == 0)
                    semaphoreSlim.Release();
            }
            return true;
        }
        public void Dispose()
        {

        }
    }
}