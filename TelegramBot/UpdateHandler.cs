﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types;
using TelegramBot.Models;

namespace TelegramBot
{
    public class UpdateHandler : IUpdateHandler
    {
        private readonly ILogger logger;
        public event EventHandler<TelegramMessageCallback> OnMessageRecived;
        public UpdateHandler(ILogger logger)
        {
            this.logger = logger;
        }
        public Task HandleErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
        {
            logger.LogError(exception, "there was an error in telegram bot client..");
            return Task.CompletedTask;
        }
        public async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
        {
            switch (update.Type)
            {
                case Telegram.Bot.Types.Enums.UpdateType.Unknown:
                    break;
                case Telegram.Bot.Types.Enums.UpdateType.Message:
                    switch (update.Message.Type)
                    {
                        case Telegram.Bot.Types.Enums.MessageType.Unknown:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.Text:
                            if (new Func<string,bool>(message => message.Contains("chatid") )(update.Message.Text.ToLower()))
                            {
                                await botClient.SendTextMessageAsync(update.Message.Chat.Id, $"Your Chat ID Is {update.Message.Chat.Id}");
                            }
                            if(new Func<string,bool>(message => message.Contains("userid"))(update.Message.Text.ToLower()))
                            {
                                await botClient.SendTextMessageAsync(update.Message.Chat.Id, $"Your User ID Is {update.Message.From.Username}");
                            }
                            OnMessageRecived?.Invoke(this, new TelegramMessageCallback() { ChatID = update.Message.Chat.Id.ToString(), Message = update.Message.Text });
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.Photo:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.Audio:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.Video:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.Voice:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.Document:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.Sticker:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.Location:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.Contact:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.Venue:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.Game:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.VideoNote:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.Invoice:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.SuccessfulPayment:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.WebsiteConnected:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.ChatMembersAdded:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.ChatMemberLeft:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.ChatTitleChanged:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.ChatPhotoChanged:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.MessagePinned:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.ChatPhotoDeleted:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.GroupCreated:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.SupergroupCreated:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.ChannelCreated:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.MigratedToSupergroup:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.MigratedFromGroup:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.Poll:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.Dice:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.MessageAutoDeleteTimerChanged:
                            break;
                        case Telegram.Bot.Types.Enums.MessageType.ProximityAlertTriggered:
                            break;
                    }
                    break;
                case Telegram.Bot.Types.Enums.UpdateType.InlineQuery:
                    break;
                case Telegram.Bot.Types.Enums.UpdateType.ChosenInlineResult:
                    break;
                case Telegram.Bot.Types.Enums.UpdateType.CallbackQuery:
                    break;
                case Telegram.Bot.Types.Enums.UpdateType.EditedMessage:
                    break;
                case Telegram.Bot.Types.Enums.UpdateType.ChannelPost:
                    break;
                case Telegram.Bot.Types.Enums.UpdateType.EditedChannelPost:
                    break;
                case Telegram.Bot.Types.Enums.UpdateType.ShippingQuery:
                    break;
                case Telegram.Bot.Types.Enums.UpdateType.PreCheckoutQuery:
                    break;
                case Telegram.Bot.Types.Enums.UpdateType.Poll:
                    break;
                case Telegram.Bot.Types.Enums.UpdateType.PollAnswer:
                    break;
                case Telegram.Bot.Types.Enums.UpdateType.MyChatMember:
                    break;
                case Telegram.Bot.Types.Enums.UpdateType.ChatMember:
                    break;
                case Telegram.Bot.Types.Enums.UpdateType.ChatJoinRequest:
                    break;
            }
        }
    }
}
