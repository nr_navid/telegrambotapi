﻿using System.ComponentModel.DataAnnotations;

namespace TelegramBotApi.Enums
{
    public enum Role
    {
        [Display(Name = "مدیر سیستم")]
        SuperAdmin,
        [Display(Name = "مدیر")]
        Admin,
        [Display(Name = "کاربر عادی")]
        NormalUser
    }
    public static class UserRole
    {
        public const string SuperAdmin = "SuperAdmin";
        public const string Admin = "Admin";
        public const string NormalUser = "NormalUser";
    }
}
