using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using NavidLogging;
using System.Text;
using TelegramBot.Models;
using Microsoft.AspNetCore.HttpOverrides;
using TelegramBotApi.Data;
using TelegramBotApi.Interfaces;
using TelegramBotApi.Models;
using TelegramBotApi.Models.Dtos;
using TelegramBotApi.Seeders;
using TelegramBotApi.Services;

var builder = WebApplication.CreateBuilder(args);
//builder.Host.ConfigureWebHostDefaults(p => p.UseUrls("http://localhost:7000", "https://localhost:8000"));
var secretKeySection = builder.Configuration.GetSection("SecretKey");
// Add services to the container.
builder.Services.Configure<SecretKey>(secretKeySection);
//var ddd = builder.Configuration.GetConnectionString("TelegramBotSystemDB");
builder.Services.Configure<List<TelegramBotConfig>>(builder.Configuration.GetSection("TelegramBotConfigs"));
builder.Services.Configure<UserDto>(builder.Configuration.GetSection("UserSeedDto"));
builder.Services.AddLogging(logbuilder => logbuilder.AddNavidLogger(options => builder.Configuration.GetSection("Logging").GetSection("NavidLogger").GetSection("Options").Bind(options)));
//builder.Services.AddDbContext<ApplicationDbContext>(options =>     // For Sql Lite
//    options.UseSqlite(builder.Configuration.GetConnectionString("TelegramBotSystemDB"))
//    );
//builder.Services.AddDbContext<ApplicationDbContext>(options => // For SQL Server
//                options.UseSqlServer(
//                    builder.Configuration.GetConnectionString("TelegramBotSystemDB")));

builder.Services.AddDbContext<ApplicationDbContext>(options => options.UseLazyLoadingProxies().UseMySql(builder.Configuration.GetConnectionString("TelegramBotSystemDBMySQL") , new MySqlServerVersion(new Version(8,0,39))));
//builder.Services.AddDatabaseDeveloperPageExceptionFilter();
builder.Services.AddIdentity<ApplicationUser, IdentityRole>(options => options.SignIn.RequireConfirmedAccount = true)
    .AddEntityFrameworkStores<ApplicationDbContext>();
builder.Services.AddAuthentication(options =>
{
options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(jwt =>
{
    jwt.Events = new JwtBearerEvents()
    {
        OnMessageReceived = (context) =>
        {
            var accessToken = context.Request.Query["access_token"];
            var path = context.HttpContext.Request.Path;
            if (string.IsNullOrEmpty(accessToken))
            {
                context.Token = accessToken;
            }
            return Task.CompletedTask;
        },
        OnTokenValidated = async context =>
        {
            var userManager = context.HttpContext.RequestServices.GetRequiredService<UserManager<ApplicationUser>>();
            var user = await userManager.FindByIdAsync(context.Principal.Identity.Name);
            if (user == null)
            {
                context.Fail("unauthorised");
            }
        }
    };
    var key = Encoding.ASCII.GetBytes(secretKeySection.Get<SecretKey>().Secret);
    jwt.RequireHttpsMetadata = false;
    jwt.SaveToken = true;
    jwt.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters()
    {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = new SymmetricSecurityKey(key),
        ValidateAudience = false,
        ValidateIssuer = false,
        ValidateLifetime = true,
        RequireExpirationTime = false
    };
});
builder.Services.AddSingleton<ITelegramAPIService>(serviceprovider => new TelegramAPIService(serviceprovider.GetRequiredService<IOptions<List<TelegramBotConfig>>>() , serviceprovider.GetRequiredService<ILogger<TelegramAPIService>>()));
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();
app.UseForwardedHeaders(new ForwardedHeadersOptions
{
    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
});
//app.Urls.Add("http://localhost:7000");
//app.Urls.Add("https://localhost:8000");
// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
//    app.UseSwagger();
//    app.UseSwaggerUI();
//}
app.UseSwagger();
app.UseSwaggerUI();

using (IServiceScope scope = app.Services.CreateScope())
{
    var usermanager = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
    var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
    var applicationDBContext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
    IOptions<UserDto> userdto = scope.ServiceProvider.GetRequiredService<IOptions<UserDto>>();
    TelegramBotDataInitialiser.SeedData(usermanager, roleManager, userdto, applicationDBContext);
}

app.Services.GetRequiredService<ITelegramAPIService>().HookUpTelegramBotForListening();

//app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
