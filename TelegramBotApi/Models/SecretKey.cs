﻿namespace TelegramBotApi.Models
{
    public class SecretKey
    {
        public string Secret { get; set; }
    }
}
