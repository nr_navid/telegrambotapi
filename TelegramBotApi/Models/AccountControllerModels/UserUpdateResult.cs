﻿using Microsoft.AspNetCore.Identity;

namespace TelegramBotApi.Models.AccountControllerModels
{
    public class UserUpdateResult
    {
        public IdentityResult IdentityResult { get; set; }
        public string UserName { get; set; }
    }
}
