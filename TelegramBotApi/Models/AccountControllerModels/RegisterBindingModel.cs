﻿using System.ComponentModel.DataAnnotations;

namespace TelegramBotApi.Models.AccountControllerModels
{
    public class RegisterBindingModel
    {
        //[Required]
        //[StringLength(15, ErrorMessage = "The {0} must be between {2} and {1} Characters", MinimumLength = 4)]
        [Display(Name = "UserName")]
        public string UserName { get; set; }
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        //[DataType(DataType.Password)]
        //[Display(Name = "Confirm password")]
        //[Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        //public string ConfirmPassword { get; set; }

        [Required]
        public bool IsActive { get; set; }

        [Required]
        public int DaysToBeActive { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
    }
}
