﻿using System.ComponentModel.DataAnnotations;

namespace TelegramBotApi.Models.AccountControllerModels
{
    public class DaysToAddUserModel
    {
        [Required]
        [MinLength(4, ErrorMessage = "user name to short")]
        public string UserNames { get; set; }
        [Required]
        [Range(1, 365, ErrorMessage = "adding days could not be more than 365 days or less than 1 days")]
        public int DaysToAdd { get; set; }
    }
}
