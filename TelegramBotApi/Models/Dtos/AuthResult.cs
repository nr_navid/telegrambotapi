﻿namespace TelegramBotApi.Models.Dtos
{
    public class AuthResult
    {
        public string Email { get; set; }
        public string Token { get; set; }
        public bool IsSuccessfull { get; set; }
        public string ErrorDescription { get; set; }
    }
}
