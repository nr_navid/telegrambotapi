﻿using System.ComponentModel.DataAnnotations;

namespace TelegramBotApi.Models.Dtos
{
    public class BaseUserDto
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
