﻿namespace TelegramBotApi.Models.Dtos
{
    public class LoggedIn
    {
        public bool IsLoggedIn { get; set; }
    }
}
