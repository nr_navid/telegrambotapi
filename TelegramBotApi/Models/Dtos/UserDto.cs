﻿using System.ComponentModel.DataAnnotations;

namespace TelegramBotApi.Models.Dtos
{
    public class UserDto : BaseUserDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public DateTime ExpireDate { get; set; }
        public DateTime CreationDate { get; set; }
        public bool Active { get; set; }
    }
}
