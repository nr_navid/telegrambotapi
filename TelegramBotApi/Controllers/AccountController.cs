﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using TelegramBotApi.Data;
using TelegramBotApi.Data.DBModels;
using TelegramBotApi.Enums;
using TelegramBotApi.Models;
using TelegramBotApi.Models.AccountControllerModels;
using TelegramBotApi.Models.Dtos;

namespace TelegramBotApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signManager;
        private readonly ILogger<AccountController> logger;
        private readonly SecretKey secret;
        public AccountController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signManager, IOptions<SecretKey> secret, ILogger<AccountController> logger)
        {
            this.userManager = userManager;
            this.signManager = signManager;
            this.logger = logger;
            this.secret = secret.Value;
        }
        private async Task<string> GenerateJwtToken(ApplicationUser user)
        {
            var roles = await userManager.GetRolesAsync(user);
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(secret.Secret);
            List<Claim> claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, user.Id.ToString())
            };
            //claims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            claims.Add(new Claim(ClaimTypes.Email, user.Email));
            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }
            ClaimsIdentity claimsIdentity = new ClaimsIdentity();
            var tokenDescriptor = new SecurityTokenDescriptor
            {

                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);
            return tokenString;
        }
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> AmILoggedIn()
        {
            return await Task.Run(() =>
            {
                if (HttpContext.User.Identity.IsAuthenticated)
                    return Ok(new LoggedIn() { IsLoggedIn = true});
                return Ok(new LoggedIn() { IsLoggedIn = false });
            });
        }
        [HttpPost]
        [Authorize(Roles = UserRole.SuperAdmin)]
        public async Task<IActionResult> Register([FromForm] RegisterBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            DateTime now = DateTime.UtcNow;
            var user = new ApplicationUser()
            {
                UserName = (string.IsNullOrEmpty(model.UserName) ? model.Email.Split("@")[0] : model.UserName),
                Email = model.Email,
                Active = model.IsActive,
                FirstName = model.FirstName,
                LastName = model.LastName,
                CreationDate = now,
                ExpireDate = now.AddDays(model.DaysToBeActive)
            };
            user.UserAddDaysHistories.Add(new UserAddDaysHistory()
            {
                CreationDate = now,
                LastExpireDate = null,
                NewExpireDate = user.ExpireDate,
                NumberOfDays = model.DaysToBeActive
            });
            var result = await userManager.CreateAsync(user, model.Password);
            await userManager.AddToRoleAsync(user, Role.NormalUser.ToString());
            if (result.Succeeded)
            {
                return Ok("User Generated");
            }
            else
            {
                return BadRequest("Generating User Had Errors... " + result.Errors.First().Description);
            }

        }
        [HttpPost]
        [Authorize(Roles = UserRole.SuperAdmin)]
        public async Task<IActionResult> AddDaysToUserName([FromForm] DaysToAddUserModel model)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            List<string> users = model.UserNames.Split(",").ToList();
            var notExistedUser = await Task.Run(() => { return users.Where(p => !userManager.Users.Select(p => p.UserName).Contains(p)).ToArray(); });
            if (notExistedUser.Any())
            {
                return BadRequest($"{string.Join(",", notExistedUser)} not exited in database");
            }
            DateTime now = DateTime.UtcNow;
            List<UserUpdateResult> identityresults = new List<UserUpdateResult>();
            foreach (var suser in users)
            {
                var seluser = await userManager.FindByNameAsync(suser);
                UserAddDaysHistory userAddDaysHistory = new UserAddDaysHistory()
                {
                    CreationDate = now,
                    LastExpireDate = seluser.ExpireDate,
                    NumberOfDays = model.DaysToAdd,

                };
                if (seluser.ExpireDate < now)
                {
                    seluser.ExpireDate = now.AddDays(model.DaysToAdd);
                    userAddDaysHistory.NewExpireDate = seluser.ExpireDate;
                }
                else
                {
                    seluser.ExpireDate = seluser.ExpireDate.AddDays(model.DaysToAdd);
                    userAddDaysHistory.NewExpireDate = seluser.ExpireDate;
                }
                seluser.UserAddDaysHistories.Add(userAddDaysHistory);
                identityresults.Add(new UserUpdateResult() { IdentityResult = await userManager.UpdateAsync(seluser), UserName = suser });
            }
            if (identityresults.Any(p => !p.IdentityResult.Succeeded))
            {
                return BadRequest(identityresults);
            }
            else
            {
                return Ok("All Users Day Added");
            }
        }
        private string GetRemoteIPAddress()
        {
            return HttpContext.Features.Get<IHttpConnectionFeature>().RemoteIpAddress.ToString();
        }
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Authenticate([FromBody] BaseUserDto userDto)
        {
            if (!ModelState.IsValid)
            {
                logger.LogError($"invalid model state - IP Address:{GetRemoteIPAddress()}");
                return BadRequest(ModelState);
            }
            var user = await userManager.FindByNameAsync(userDto.Username); //;
            if (user is null)
            {
                logger.LogError($"Username is incorrect - IP Address:{GetRemoteIPAddress()}");
                return BadRequest(new AuthResult() { IsSuccessfull = false, ErrorDescription = "Username or password is incorrect" });
            }     
            var result = await userManager.CheckPasswordAsync(user, userDto.Password);
            if (!result)
            {
                logger.LogError($"Password is incorrect - IP Address:{GetRemoteIPAddress()}");
                return BadRequest(new AuthResult() { IsSuccessfull = false, ErrorDescription = "Username or password is incorrect" });
            }        
            if (user.ExpireDate < DateTime.UtcNow)
            {
                logger.LogError($"Account Expierd - IP Address:{GetRemoteIPAddress()}");
                return BadRequest(new AuthResult() { IsSuccessfull = false, ErrorDescription = "Your Account Has Been Expierd." });
            }
            if (!user.Active)
            {
                logger.LogError($"Account Not Active - IP Address:{GetRemoteIPAddress()}");
                return BadRequest(new AuthResult() { IsSuccessfull = false, ErrorDescription = "Your User Is disabled By Administrator" });
            }
            // return basic user info (without password) and token to store client side
            return Ok(new AuthResult()
            {
                Email = user.Email,
                IsSuccessfull = true,
                Token = await GenerateJwtToken(user)
            });
        }
    }
}
