using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NavidCoreTools.Enums.API;
using NavidCoreTools.Models.Telegram;
using System.Security.Claims;
using TelegramBotAPI.Models;
using TelegramBotApi.Data;
using TelegramBotApi.Enums;
using TelegramBotApi.Interfaces;

namespace TelegramBotApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class TelegramController : ControllerBase
    {
        private readonly ITelegramAPIService telegramAPIService;
        private readonly ApplicationDbContext dbContext;
        private readonly UserManager<ApplicationUser> userManager;

        public TelegramController(ITelegramAPIService telegramAPIService, ApplicationDbContext dbContext , UserManager<ApplicationUser> userManager)
        {
            this.telegramAPIService = telegramAPIService;
            this.dbContext = dbContext;
            this.userManager = userManager;
        }
        private async Task<APIResault<object>> UserValidToGo(object messageBase)
        {
            var user = await userManager.FindByIdAsync(HttpContext.User.Identity.Name);
            if (HttpContext.User.Claims.Any( p => p.Type == ClaimTypes.Role && p.Value == UserRole.SuperAdmin))
                return new APIResault<object>();
            if(user.Active && user.ExpireDate > DateTime.Now)
            {
                if (messageBase is TelegramMessageBase)
                {
                    var tMessageBase = (TelegramMessageBase)messageBase;
                    if(user.UserAvailableChatIDs.Any(p => p.ChatID == tMessageBase.EnvironmentID && p.Active) && user.UserAvailableTelegramBots.Any(p => p.Active && p.TelegramBotName == tMessageBase.TelegramBotName))
                    {
                        return new APIResault<object>();
                    }
                    else
                    {
                        return new APIErrorResault<object>("Your User Does Not Have Permission To Send Message TO this bot or this Chat", null, false);
                    }
                }
                else if (messageBase is List<TelegramMessageBase>)
                {
                    var tMessageBases = (List<TelegramMessageBase>)messageBase;
                    if (tMessageBases.Any(p => !user.UserAvailableTelegramBots.Where(p => p.Active).Select(p => p.TelegramBotName).Contains(p.TelegramBotName)) && tMessageBases.Any(p => !user.UserAvailableChatIDs.Where(p => p.Active).Select(p => p.ChatID).Contains(p.EnvironmentID)))
                    {
                        return new APIErrorResault<object>("Your User Does Not Have Permission To Send Message TO this bots or this Chats that you specified", null, false);
                    }
                    else
                    {
                        return new APIResault<object>();
                    }
                }
                else
                {
                    return new APIErrorResault<object>("Invalid Request Data..", null, false);
                }
            }
            else
            {
                return new APIErrorResault<object>("Your User Is Not Active Please Contact Navid RJ", null, false);
            }
        }
        [HttpPost]
        public async Task<IActionResult> SendMessageAndWaitForResponce([FromForm] TelegramMessageBase telegramMessageBase)
        {
            if (ModelState.IsValid)
            {
                var userAllowed = await UserValidToGo(telegramMessageBase);
                if (userAllowed.APIStatus == APIStatus.Success)
                {
                    var resault = await telegramAPIService.SendMessageToTelegramAndWaitForResponse(telegramMessageBase);
                    switch (resault.APIStatus)
                    {
                        case APIStatus.Success:
                            return Ok(resault);
                        case APIStatus.Error:
                            return StatusCode(500, resault);
                    }
                    return this.BadRequest();
                }
                else
                {
                    return BadRequest(userAllowed);
                }

            }
            else
            {
                return BadRequest(new APIErrorResault<object>("Model Is Not Valid"));
            }

        }
        [HttpPost]
        public async Task<IActionResult> SendMessage([FromForm] TelegramMessageBase telegramMessageBase)
        {
            if (ModelState.IsValid)
            {
                var userAllowed = await UserValidToGo(telegramMessageBase);
                if(userAllowed.APIStatus == APIStatus.Success)
                {
                    var resault = await telegramAPIService.SendMessageViaTelegramAsync(telegramMessageBase);
                    switch (resault.APIStatus)
                    {
                        case APIStatus.Success:
                            return Ok(resault);
                        case APIStatus.Error:
                            return StatusCode(500, resault);
                    }
                    return this.BadRequest();
                }
                else
                {
                    return BadRequest(userAllowed);
                }
                
            }
            else
            {
                return BadRequest(new APIErrorResault<object>("Model Is Not Valid"));
            }

        }
        [HttpPost]
        public async Task<IActionResult> SendMessageList(List<TelegramMessageBase> telegramMessageBaseList)
        {
            if (ModelState.IsValid)
            {
                var userAllowed = await UserValidToGo(telegramMessageBaseList);
                if (userAllowed.APIStatus == APIStatus.Success)
                {
                    var resault = await telegramAPIService.SendMessageViaTelegramAsync(telegramMessageBaseList);
                    switch (resault.APIStatus)
                    {
                        case APIStatus.Success:
                            return Ok(resault);
                        case APIStatus.Error:
                            return StatusCode(500, resault);
                    }
                    return this.BadRequest();
                }
                else
                {
                    return BadRequest(userAllowed);
                }
            }
            else
            {
                return BadRequest(new APIErrorResault<object>("Model Is Not Valid"));
            }
        }
    }
}