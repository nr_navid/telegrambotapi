﻿using Microsoft.Extensions.Options;
using NavidCoreTools.Models.Telegram;
using TelegramBot;
using TelegramBot.Models;
using TelegramBotApi.Interfaces;
using TelegramBotAPI.Models;

namespace TelegramBotApi.Services
{
    public class TelegramAPIService : ITelegramAPIService
    {
        private readonly Dictionary<TelegramBotConfig, TelegramBotAgent> telegramBotDict;
        private readonly ILogger logger;

        public TelegramAPIService(IOptions<List<TelegramBotConfig>> telegromBotConfigs, ILogger<TelegramAPIService> logger)
        {
            telegramBotDict = new Dictionary<TelegramBotConfig, TelegramBotAgent>();
            foreach (var telegrambotconfig in telegromBotConfigs.Value)
            {
                TelegramBotAgent telegramBotAgent = new TelegramBotAgent(telegrambotconfig, logger);
                telegramBotDict.Add(telegrambotconfig, telegramBotAgent);
            }
            this.logger = logger;
        }
        public void HookUpTelegramBotForListening()
        {
            foreach (var bot in telegramBotDict.Values)
            {
                bot.HookUpListening();
            }
        }
        public async Task<APIResault<object>> SendMessageViaTelegramAsync(TelegramMessageBase telegramMessageBase)
        {
            TelegramBotAgent telegramBotAgent = telegramBotDict.FirstOrDefault(p => p.Key.TelegramBotName == telegramMessageBase.TelegramBotName).Value;
            if (telegramBotAgent == null)
            {
                logger.LogError($"telegram Bot {telegramMessageBase.TelegramBotName} Expected Does Not Exist", telegramMessageBase);
                return new APIErrorResault<object>(message: "message has been faild to send. the bot selected does not exists") { APIStatus = NavidCoreTools.Enums.API.APIStatus.Error };
            }
            bool sent = await telegramBotAgent?.SendMessageAsync(new TelegramMessage(telegramMessageBase));
            if (sent)
            {
                return new APIResault<object> { APIStatus = NavidCoreTools.Enums.API.APIStatus.Success, Message = "Successfully Sent." };
            }
            else
            {
                logger.LogError($"telegram Bot {telegramMessageBase.TelegramBotName} Expected Does Not Exist", telegramMessageBase);
                return new APIErrorResault<object>(message: "Message Was Not Sent Trough Telegram ;(") { APIStatus = NavidCoreTools.Enums.API.APIStatus.Error };
            }
        }
        public async Task<APIResault<object>> SendMessageViaTelegramAsync(List<TelegramMessageBase> telegramMessageBaseList)
        {
            foreach (var tmessage in telegramMessageBaseList)
            {
                TelegramBotAgent telegramBotAgent = telegramBotDict.FirstOrDefault(p => p.Key.TelegramBotName == tmessage.TelegramBotName).Value;
                if (telegramBotAgent == null)
                {
                    logger.LogError($"telegram Bot {tmessage.TelegramBotName} Expected Does Not Exist", tmessage);
                    return new APIErrorResault<object>(message: "one or more message has been faild to send. the bot selected does not exists") { APIStatus = NavidCoreTools.Enums.API.APIStatus.Error };
                }
                bool sent = await telegramBotAgent?.SendMessageAsync(new TelegramMessage(tmessage));
            }
            return new APIResault<object> { APIStatus = NavidCoreTools.Enums.API.APIStatus.Success, Message = "Successfully Sent." };
        }
        public async Task<APIResault<string>> SendMessageToTelegramAndWaitForResponse (TelegramMessageBase telegramMessageBase)
        {
            DateTime timeSent = DateTime.Now;
            bool MessageRecived = false;
            string Message = string.Empty;
            TelegramBotAgent telegramBotAgent = telegramBotDict.FirstOrDefault(p => p.Key.TelegramBotName == telegramMessageBase.TelegramBotName).Value;
            if (telegramBotAgent == null)
            {
                logger.LogError($"telegram Bot {telegramMessageBase.TelegramBotName} Expected Does Not Exist", telegramMessageBase);
                return new APIErrorResault<string>(message: "message has been faild to send. the bot selected does not exists") { APIStatus = NavidCoreTools.Enums.API.APIStatus.Error };
            }
            bool sent = await telegramBotAgent?.SendMessageAsync(new TelegramMessage(telegramMessageBase));
            if (sent)
            {
                EventHandler<TelegramMessageCallback> handler = async (object? obj, TelegramMessageCallback message) =>
                {
                    if (telegramMessageBase.EnvironmentID == message.ChatID)
                    {
                        Message = message.Message;
                        MessageRecived = true;
                    }
                };
                telegramBotAgent.OnMessageReceived += handler;
                while(!MessageRecived && timeSent.AddMinutes(5) > DateTime.Now)
                {
                    await Task.Delay(10000);
                }
                telegramBotAgent.OnMessageReceived -= handler;
                if (MessageRecived)
                {
                    return new APIResault<string> { APIStatus = NavidCoreTools.Enums.API.APIStatus.Success, Message = "Successfully Sent.", data = Message };
                }
                else
                {
                    return new APIResault<string> { APIStatus = NavidCoreTools.Enums.API.APIStatus.Error, Message = $"User With ChatID of {telegramMessageBase.EnvironmentID} Didnt Respond To Message", data = Message };
                }

                
            }
            else
            {
                logger.LogError($"telegram Bot {telegramMessageBase.TelegramBotName} Expected Does Not Exist", telegramMessageBase);
                return new APIErrorResault<string>(message: "Message Was Not Sent Trough Telegram ;(") { APIStatus = NavidCoreTools.Enums.API.APIStatus.Error };
            }
        }
    }
}
