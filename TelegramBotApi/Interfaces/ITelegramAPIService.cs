﻿using NavidCoreTools.Models.Telegram;
using TelegramBotAPI.Models;

namespace TelegramBotApi.Interfaces
{
    public interface ITelegramAPIService
    {
        Task<APIResault<object>> SendMessageViaTelegramAsync(TelegramMessageBase telegramMessageBase);
        Task<APIResault<object>> SendMessageViaTelegramAsync(List<TelegramMessageBase> telegramMessageBaseList);
        Task<APIResault<string>> SendMessageToTelegramAndWaitForResponse(TelegramMessageBase telegramMessageBase);
        void HookUpTelegramBotForListening();
    }
}
