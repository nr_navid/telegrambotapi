﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TelegramBotApi.Data.DBModels
{
    public class UserAddDaysHistory
    {
        [Key]
        public long ID { get; set; }
        public int NumberOfDays { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? LastExpireDate { get; set; }
        public DateTime NewExpireDate { get; set; }
        public string ApplicationUserID { get; set; }
        [ForeignKey("ApplicationUserID")]
        public virtual ApplicationUser User { get; set; }
    }
}
