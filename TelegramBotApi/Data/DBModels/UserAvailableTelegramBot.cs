﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TelegramBotApi.Data.DBModels
{
    public class UserAvailableTelegramBot
    {
        [Key]
        public long Id { get; set; }
        public string TelegramBotName { get; set; }
        public string UserID { get; set; }
        public bool Active { get; set; }
        [ForeignKey("UserID")]
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
