﻿using Microsoft.AspNetCore.Identity;
using TelegramBotApi.Data.DBModels;

namespace TelegramBotApi.Data
{
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {

        }
        public bool Active { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public virtual ICollection<UserAddDaysHistory> UserAddDaysHistories { get; set; }
        public virtual ICollection<UserAvailableChatID> UserAvailableChatIDs { get; set; }
        public virtual ICollection<UserAvailableTelegramBot> UserAvailableTelegramBots { get; set; }
    }
}
