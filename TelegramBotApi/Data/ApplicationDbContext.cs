﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using TelegramBotApi.Data.DBModels;

namespace TelegramBotApi.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions dbContextOptions) : base(dbContextOptions)
        {

        }
        public DbSet<UserAvailableChatID> UserAvailableChatIDs { get; set; }
        public DbSet<UserAvailableTelegramBot> UserAvailableTelegramBots { get; set; }
        public DbSet<UserAddDaysHistory> UserAddDaysHistories { get; set; }
    }
}
