﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using TelegramBotApi.Data;
using TelegramBotApi.Enums;
using TelegramBotApi.Models.Dtos;

namespace TelegramBotApi.Seeders
{
    public class TelegramBotDataInitialiser
    {
        public static void SeedData(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IOptions<UserDto> userDto, ApplicationDbContext applicationDbContext)
        {
            //applicationDbContext.Database.EnsureDeletedAsync().GetAwaiter().GetResult();
            applicationDbContext.Database.Migrate();
            // Defining Roles
            if (!roleManager.RoleExistsAsync(Role.SuperAdmin.ToString()).Result)
            {
                IdentityRole role = new IdentityRole();
                role.Name = "SuperAdmin";
                role.NormalizedName = "مدیر سیستم";
                IdentityResult roleResult = roleManager.
                CreateAsync(role).Result;
            }
            if (!roleManager.RoleExistsAsync(Role.Admin.ToString()).Result)
            {
                IdentityRole role = new IdentityRole();
                role.Name = "Admin";
                role.NormalizedName = "مدیر";
                IdentityResult roleResult = roleManager.
                CreateAsync(role).Result;
            }
            if (!roleManager.RoleExistsAsync(Role.NormalUser.ToString()).Result)
            {
                IdentityRole role = new IdentityRole();
                role.Name = "NormalUser";
                role.NormalizedName = "کاربر عادی";
                IdentityResult roleResult = roleManager.
                CreateAsync(role).Result;
            }
            // Defining Initializer Users
            if (userManager.FindByNameAsync(userDto.Value.Username).Result == null)
            {
                ApplicationUser omsUser = new ApplicationUser()
                {
                    UserName = userDto.Value.Username,
                    Email = userDto.Value.Email,
                    ExpireDate = userDto.Value.ExpireDate,
                    Active = true,
                    CreationDate = DateTime.Now,
                    FirstName = userDto.Value.FirstName,
                    LastName = userDto.Value.LastName
                };
                IdentityResult result = userManager.CreateAsync(omsUser, userDto.Value.Password).Result;
                if (result.Succeeded)
                {
                    userManager.UpdateAsync(omsUser).Wait();
                    userManager.AddToRoleAsync(omsUser, Role.SuperAdmin.ToString()).Wait();
                }
            }
        }
    }
}
